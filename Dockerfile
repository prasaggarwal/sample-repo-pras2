FROM openjdk:8-jdk-alpine AS build
WORKDIR /srv
COPY    . .
RUN     ./gradlew test build

FROM openjdk:8-jdk-alpine
EXPOSE 8080
WORKDIR /app
COPY --from=build /srv/ /app/
ENTRYPOINT ["java","-jar","build/libs/spring-boot-0.0.1-SNAPSHOT.jar"]